#!/usr/bin/env python3
""" Class for pioche - 1 packet of masked cards + 3 cards """
import objet_pile

## resultat object
#
class ObjetPioche():
    """" ObjetPioche - This object defines the pioche object """
    M_PIOCHE_CARD_NUMBERS = 24
    M_VISIBLE_CARD_NUMBERS = 3
    _m_pioche_pile = None
    _m_pioche_visible = None

    ## constructor
    #
    def __init__(self, my_jeu_52_cartes):
        """" constructor """
        # create the pioche
        self._m_pioche_pile = objet_pile.ObjetPile(my_jeu_52_cartes, self.M_PIOCHE_CARD_NUMBERS)
        self._m_pioche_visible = objet_pile.ObjetPile(my_jeu_52_cartes)

    ## destructor
    #
    def __del__(self):
        """ destructor """

    ## Display the pioche cards
    #
    def affiche_pioche(self):
        """ Display the pioche cards """
        print("----- PIOCHE CONTENT (" + str(self._m_pioche_pile.how_many_cards()) + ") -----")
        # show only last 3 visible cards
        visible_card_nb = self._m_pioche_visible.how_many_cards()
        for index in range(min(self.M_VISIBLE_CARD_NUMBERS, visible_card_nb)):
            self._m_pioche_visible.what_is_card(index).afficher_une_carte()

    ## ask last card from pioche without removing it
    # @return Retreive the last card from pioche
    def get_last_card(self):
        """ Get last card from pioche """
        return self._m_pioche_visible.what_is_card()

    ## Pop last card from pioche
    # @return Pop the last card from pioche
    def pop_last_card(self):
        """ Pop last card from pioche """
        return self._m_pioche_visible.pop_last_card()

    ## Use pioche
    #
    def piocher(self):
        """ use pioche """
        if self._m_pioche_pile.how_many_cards():
            # pioche a card
            the_card = self._m_pioche_pile.pop_last_card()
            the_card.rendre_visible()
            self._m_pioche_visible.push_card(the_card)
        else:
            # pioche is empty, get it back
            for _ in range(self._m_pioche_visible.how_many_cards()):
                the_card = self._m_pioche_visible.pop_last_card()
                the_card.masquer_carte()
                self._m_pioche_pile.push_card(the_card)
