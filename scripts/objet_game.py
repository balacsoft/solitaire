""" Class for Jeu52Cartes - 52 cards """
import resultat
import pioche
import tapis_joueur
import jeu_52_cartes
import objet_pile

## Game object
#
class GameObject():
    """" Game object """
    _m_jeu_52_cartes = None
    _m_jeu_pioche = None
    _m_jeu_tapis_joueur = None
    _m_jeu_resultat = None

    # public methods

    ## constructor
    #
    def __init__(self):
        """" constructor """
        print("\nCREATE GAME CARDS")
        self._m_jeu_52_cartes = jeu_52_cartes.Jeu52Cartes()
        print("\nGET PIOCHE CARDS (24 cards)")
        self._m_jeu_pioche = pioche.ObjetPioche(self._m_jeu_52_cartes)   
        print("\nCREATE RESULTAT")
        self._m_jeu_resultat = resultat.ObjetResultat(self._m_jeu_52_cartes)
        print("\nGET TABLE JOUEUR CARDS (28 cards)")
        self._m_jeu_tapis_joueur = tapis_joueur.ObjetTapisJoueur(self._m_jeu_52_cartes)

    ## destructor
    #
    def __del__(self):
        """ destructor """

    ## StartGame
    #
    def start_game(self):
        """ Game Main loop """

        # start game
        print("---- START GAME ----")

        continue_game = True
        while continue_game:
            print(" --- ")
            self._m_jeu_tapis_joueur.display_player_area()
            self._m_jeu_pioche.affiche_pioche()
            self._m_jeu_resultat.affiche_resultat()
            player_input = input("Select your action (Q, P, Gx, Rxy, Txy, Dx)>")
            action = player_input.upper()
            continue_game = self.__manage_action(action)

    # private methods

    ## Manage Player actions
    #
    def __manage_action(self, the_action):
        """ Manage player action """
        cont = True
        if the_action == "":
            pass
        elif the_action[0] == "Q":
            cont = False
        elif the_action[0] == "P":
            # pioche
            self._m_jeu_pioche.piocher()
        elif the_action[0] == "G":
            # G = from 'pioche' to 'resultat'
            numero_pile = int(the_action[1]) - 1
            the_card = self._m_jeu_pioche.get_last_card()
            if self._m_jeu_resultat.push_card(the_card, numero_pile):
                the_card = self._m_jeu_pioche.pop_last_card()
            else:
                print("Insertion not possible in " + the_action)
        elif the_action[0] == "R":
            # R = from 'tapis' to 'resultat'
            src_pile = int(the_action[1]) - 1
            dest_pile = int(the_action[2]) - 1
            the_card = self._m_jeu_tapis_joueur.get_last_card(src_pile)
            if self._m_jeu_resultat.push_card(the_card, dest_pile):
                self._m_jeu_tapis_joueur.pop_last_card(src_pile)
                next_card = self._m_jeu_tapis_joueur.get_last_card(src_pile)
                if next_card:
                    next_card.rendre_visible()
            else:
                print("Insertion not possible in " + the_action)
        elif the_action[0] == "T":
            # T = from 'tapis' to 'tapis'
            src_pile = int(the_action[1]) - 1
            dest_pile = int(the_action[2]) - 1
            the_card = self._m_jeu_tapis_joueur.get_last_card(src_pile)
            if self._m_jeu_tapis_joueur.push_card(the_card, dest_pile):
                self._m_jeu_tapis_joueur.pop_last_card(src_pile)
                next_card = self._m_jeu_tapis_joueur.get_last_card(src_pile)
                if next_card:
                    next_card.rendre_visible()
            else:
                print("Insertion not possible in " + the_action)
        elif the_action[0] == "D":
            # D = from 'pioche' to 'tapis'
            dest_pile = int(the_action[1]) - 1
            the_card = self._m_jeu_pioche.get_last_card()
            if self._m_jeu_tapis_joueur.push_card(the_card, dest_pile):
                self._m_jeu_pioche.pop_last_card()
                next_card = self._m_jeu_tapis_joueur.get_last_card(dest_pile)
                if next_card:
                    next_card.rendre_visible()
            else:
                print("Insertion not possible in " + the_action)

        # check for victory
        if self._m_jeu_resultat.is_victory():
            # VICTORY
            print("VICTORY !!!!!!")
            cont = False
        return cont
