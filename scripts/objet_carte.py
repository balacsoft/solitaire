""" Class for card object """

## Color of the cards
#
class CarteColor:
    """" Color of the card """
    # pylint: disable=too-few-public-methods
    no_color = 0
    red = 1
    black = 2

## Symbols of the cards
#
class CarteSymbole:
    """" Symbols of the card """
    # pylint: disable=too-few-public-methods
    empty = 0
    coeur = 1
    carreau = 2
    trefle = 3
    pique = 4

## Value of the cards
#
class CarteValeur:
    """" Value of the card """
    # pylint: disable=too-few-public-methods
    carte_empty = 0
    carte_as = 1
    carte_2 = 2
    carte_3 = 3
    carte_4 = 4
    carte_5 = 5
    carte_6 = 6
    carte_7 = 7
    carte_8 = 8
    carte_9 = 9
    carte_10 = 10
    carte_valet = 11
    carte_dame = 12
    carte_roi = 13

## Card object
#
class ObjetCarte():
    """" ObjetCarte - This object defines a playing card """
    _m_carte_visible = False
    _m_carte_symbole = CarteSymbole.empty
    _m_carte_valeur = CarteValeur.carte_empty
    _m_localisation = 0

    # public functions

    ## constructor
    #
    def __init__(self, symb=CarteSymbole.empty, val=CarteValeur.carte_empty, visue=False, loc=None):
        """" constructor """
        self._m_carte_visible = visue
        self._m_carte_symbole = symb
        self._m_carte_valeur = val
        self._m_localisation = loc

    ## destructor
    #
    def __del__(self):
        """ destructor """

    ## Display a card
    #
    def afficher_une_carte(self, pre_fixe=""):
        """ display a card """
        if self._m_carte_visible:
            print(pre_fixe + self.__s_carte_value() + "-" + self.__s_carte_symbole())

    ## move a card
    # @param localisation New place for the card
    # @return New localisation or None if movement not possible
    def deplacer_une_carte(self, localisation):
        """ move a card from current position to another """
        if self._m_carte_visible is False:
            # impossible to move card if it is not visible
            return None

        self._m_localisation = localisation

        # return new localisation
        return localisation

    ## Get symbol of the card
    # @return returns the symbol of the card
    def get_symbol(self):
        """ Get symbol of the card """
        return self._m_carte_symbole

    ## Get color of the card
    # @return returns the color of the card
    def get_color(self):
        """ Get color of the card """
        output = CarteColor.black
        if self._m_carte_symbole == CarteSymbole.carreau or self._m_carte_symbole == CarteSymbole.coeur:
            output = CarteColor.red
        return output

    ## Get value of the card
    # @return returns the value of the card
    def get_value(self):
        """ Get value of the card """
        return self._m_carte_valeur

    ## make the card visible
    #
    def rendre_visible(self):
        """ make the card visible """
        self._m_carte_visible = True

    ## make the card invisible
    #
    def masquer_carte(self):
        """ make the card invisible """
        self._m_carte_visible = False

    # privtate functions

    ## Retrieve a string with the card value
    # @return String containing a readable value for the card
    def __s_carte_value(self):
        """ Retrieve a string with the card value """
        str_card = str(self._m_carte_valeur)
        if str_card == "1":
            str_card = "as"
        elif str_card == "11":
            str_card = "Valet"
        elif str_card == "12":
            str_card = "Dame"
        elif str_card == "13":
            str_card = "Roi"
        elif str_card == "0":
            str_card = "No_card"
        return str_card

    ## Retrieve a string with the card symbole
    # @return String containing a readable symbol for the card
    def __s_carte_symbole(self):
        """ Retrieve a string with the card symbole """
        str_card = str(self._m_carte_symbole)
        if str_card == "1":
            str_card = "Coeur"
        elif str_card == "2":
            str_card = "Carreau"
        elif str_card == "3":
            str_card = "Trefle"
        elif str_card == "4":
            str_card = "Pique"
        elif str_card == "0":
            str_card = "No_card"
        return str_card
