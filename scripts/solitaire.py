#!/usr/bin/env python3
""" solitaire game """
import objet_game

def solitaire_main():
    """" solitaire """
    my_solitaire = objet_game.GameObject()
    my_solitaire.start_game()

# Main function
if __name__ == '__main__':
    solitaire_main()
