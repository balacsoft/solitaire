""" Class for resultat - 4 packets of cards """
from jeu_52_cartes import Jeu52Cartes
import objet_carte
import objet_pile

## resultat object
#
class ObjetResultat():
    """" ObjetResultat - This object defines the resultat object """
    C_PILE_NUMBER = 4
    _m_resultas = [] # 4 objects of type pile
    _nb_cards = 0

    # public functions

    ## constructor
    #
    def __init__(self, my_jeu_52_cartes):
        """" constructor """
        for _ in range(self.C_PILE_NUMBER):
            self._m_resultas.append(objet_pile.ObjetPile(my_jeu_52_cartes, 0))

    ## destructor
    #
    def __del__(self):
        """ destructor """

    ## Push a card at correct place
    # @param the_card The card to insert in resultat
    # @param the_pile The pile where to insert the card
    # @return True if card is pushed, False otherwise
    def push_card(self, the_card, the_pile):
        """ Push a card at correct place """
        push_statut = False
        # first look if same symbol already exists
        the_symbol = self.__get_symbol_of_pile(the_pile)
        if the_card.get_symbol() == the_symbol:
            # push the card, if value = +1 only
            the_value = self.__get_value_of_pile(the_pile)
            if the_card.get_value() == the_value + 1:
                self._m_resultas[the_pile].push_card(the_card)
                push_statut = True
        elif the_symbol == objet_carte.CarteValeur.carte_empty:
            # push if as only
            if the_card.get_value() == objet_carte.CarteValeur.carte_as:
                self._m_resultas[the_pile].push_card(the_card)
                push_statut = True
        return push_statut

    ## method to check if the player has won
    # @return Returns True if player has won, False otherwise
    def is_victory(self):
        """ method to check if the player has won """
        my_result = bool(self._nb_cards == Jeu52Cartes.NB_GAME_CARDS)
        print("is_victory=" + str(my_result))
        return my_result

    ## Display resultat cards
    #
    def affiche_resultat(self):
        """ Display resultat cards """
        print("------ RESULTAT CONTENT (" + str(self._nb_cards) + ") -----")
        for the_pile in range(self.C_PILE_NUMBER):
            the_card = self.__get_last_card_of_pile(the_pile)
            if the_card is not None:
                the_card.afficher_une_carte(str(the_pile + 1) + " - ")

    # private functions

    ## Retreive the symbol for a given pile
    # @param pile The pile for which the symbol is requested
    # @return Symbol for the given pile, None if pile is empty
    def __get_symbol_of_pile(self, the_pile):
        """ Retreive the symbol for a give pile """
        output = objet_carte.CarteSymbole.empty
        if self._m_resultas[the_pile]:
            if self._m_resultas[the_pile].how_many_cards():
                the_card = self._m_resultas[the_pile].what_is_card()
                output = the_card.get_symbol()
        return output

    ## Retreive the value of last card of the pile
    # @param pile The pile for which the value is requested
    # @return Value for the last card of given pile, None if pile is empty
    def __get_value_of_pile(self, the_pile):
        """ Retreive the value of last card of the pile """
        output = objet_carte.CarteValeur.carte_empty
        if self._m_resultas[the_pile]:
            if self._m_resultas[the_pile].how_many_cards():
                the_card = self._m_resultas[the_pile].what_is_card()
                output = the_card.get_value()
        return output

    ## Retreive the last card of a given pile
    # @param pile The pile for which the card is requested
    # @return Card for the given pile, None if pile is empty
    def __get_last_card_of_pile(self, the_pile):
        """ Retreive the last card of a given pile """
        output = None
        if self._m_resultas[the_pile]:
            output = self._m_resultas[the_pile].what_is_card()
        return output
