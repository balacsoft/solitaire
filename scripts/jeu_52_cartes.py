""" Class for Jeu52Cartes - 52 cards """
import random
import objet_carte
import objet_pile

## Jeu52Cartes object
#
class Jeu52Cartes():
    """" Jeu52Cartes - This object defines a game of 52 cards """

    # pubic members
    NB_GAME_CARDS = 52
    m_jeu_52_cartes = []
    # private members
    __SPLIT_NUMBER = NB_GAME_CARDS / 4 # 4 symbols for all cards
    __m_jeu_carte_par_index = []
    __nb_cards_available = 0
    __current_card_position = 0

    # public methods

    ## constructor
    #
    def __init__(self):
        """" constructor """
        self.__create_random_card_game()

    ## destructor
    #
    def __del__(self):
        """ destructor """

    ## Return some cards from the current mixed card game
    # @return Return a pile object containing the requested number of cards, None otherwise
    def get_some_cards(self, number_of_cards):
        """ Return a series of cards from the 52 cards game """
        pile_to_provide = objet_pile.ObjetPile(self.m_jeu_52_cartes)
        if number_of_cards > 0:
            if number_of_cards <= self.__nb_cards_available:
                # enough cards to provide
                for index in range(number_of_cards):
                    my_card = self.m_jeu_52_cartes[index + self.__current_card_position]
                    pile_to_provide.push_card(my_card)

                # update counters
                self.__current_card_position = self.__current_card_position + number_of_cards
                self.__nb_cards_available = self.__nb_cards_available - number_of_cards
            else:
                # not enough cards to provide
                print("Card not enough!")
                return None
        return pile_to_provide.get_pile()

    ## Display the cards
    #
    def affiche_all_cards(self):
        """ Display all the cards """
        for the_card in self.m_jeu_52_cartes:
            the_card.afficher_une_carte()

    # Private methods

    ## mix the cards
    #
    def __create_random_card_game(self):
        """ Create a 52 cards game randomly """
        # populate the buffer
        my_random_val = None
        for _ in range(self.NB_GAME_CARDS):
            while self.__card_already_present(my_random_val):
                my_random_val = random.randrange(self.NB_GAME_CARDS)
            self.__m_jeu_carte_par_index.append(my_random_val)

        # create game
        for card_value in self.__m_jeu_carte_par_index:
            the_symbol = int(card_value / self.__SPLIT_NUMBER) + 1
            the_card_value = int(card_value % self.__SPLIT_NUMBER) + 1
            the_card = objet_carte.ObjetCarte(the_symbol, the_card_value, False, 0)
            self.m_jeu_52_cartes.append(the_card)

        # reset available card numbers
        self.__nb_cards_available = self.NB_GAME_CARDS

    ## Check if a card is already present in the card game
    # @return True if the card is already present, False otherwise
    def __card_already_present(self, the_card):
        """ Check if a card is already present in the card game """
        game_size = len(self.__m_jeu_carte_par_index)
        already_present = False
        if the_card is not None:
            for index in range(game_size):
                if the_card == self.__m_jeu_carte_par_index[index]:
                    already_present = True
                    break
        else:
            already_present = True

        return already_present
