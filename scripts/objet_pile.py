""" Class for pile object """

## Pile object
#
class ObjetPile():
    """" ObjetPile - This is a pile of cards (several cards) """
    _m_pile = []

    # public functions

    ## constructor
    #
    def __init__(self, the_52_cards, the_pile_nb=None):
        """" constructor """
        if the_pile_nb:
            self._m_pile = the_52_cards.get_some_cards(the_pile_nb)
        else:
            self._m_pile = []

    ## destructor
    #
    def __del__(self):
        """ destructor """

    ## Display visible cards of the pile
    #
    def affiche_pile(self):
        """ display the pile content """
        if self._m_pile is not None:
            for the_card in self._m_pile:
                the_card.afficher_une_carte()
        else:
            print("Pile empty!")

    ## adds the given card in the pile
    # @param the_card The card to push in the pile
    def push_card(self, the_card):
        """ adds the given card in the pile """
        self._m_pile.append(the_card)

    ## Get the last card and remove it from pile
    # @return Last card of the pile
    def pop_last_card(self):
        """ Get the last card and remove it from pile """
        output = None
        card_nb = self.how_many_cards()
        if card_nb > 0:
            output = self._m_pile.pop()
        return output

    ## Make the last card visible
    # 
    def make_last_visible(self):
        """ Make the last card visible """
        card_nb = self.how_many_cards()
        if card_nb > 0:
            self._m_pile[card_nb - 1].rendre_visible()

    ## Tells what is the last card of the pile
    # @return Returns a card object without removing it from the pile, None if no card available
    def what_is_card(self, at_index=0):
        """ Tells what is the last card of the pile """
        output = None
        card_nb = self.how_many_cards()
        if card_nb > at_index:
            output = self._m_pile[card_nb - at_index - 1]
        return output

    ## Returns the number of cards in the pile
    # @return Returns the number of cards in the pile
    def how_many_cards(self):
        """ Returns the number of cards in the pile """
        return len(self._m_pile)

    ## Returns the pile objet
    # @return Pile objet
    def get_pile(self):
        """ Returns the pile objet """
        return self._m_pile
