#!/usr/bin/env python3
""" Class for TapisJoueur - 7 packets of cards """
import objet_pile
import objet_carte

## TapisJoueur object
#
class ObjetTapisJoueur():
    """" ObjetTapisJoueur - This object defines the TapisJoueur object """
    C_TAPIS_JOUEUR_NUMBER = 7
    _m_tapis_joueur = []

    ## constructor
    #
    def __init__(self, my_jeu_52_cartes):
        """" constructor """
        # create the player table
        for pile in range(self.C_TAPIS_JOUEUR_NUMBER):
            the_pile_obj = objet_pile.ObjetPile(my_jeu_52_cartes, pile + 1)
            # make the first card visible
            the_card = the_pile_obj.what_is_card()
            the_card.rendre_visible()
            self._m_tapis_joueur.append(the_pile_obj)

    ## destructor
    #
    def __del__(self):
        """ destructor """

    ## Display the tapis cards
    #
    def display_player_area(self):
        """ Display the tapis cards """
        print("TAPIS CONTENT")
        for the_pile in range(self.C_TAPIS_JOUEUR_NUMBER):
            print("--- Pile %s (size=%s) ---" % (str(the_pile + 1 ), \
                self._m_tapis_joueur[the_pile].how_many_cards()))
            self._m_tapis_joueur[the_pile].affiche_pile()

    ## Get last card from tapis joueur for provided 'pile'
    # @param the_pile The 'pile' fot which a card is requested
    # @return Get the last card from the given 'pile'
    def get_last_card(self, the_pile):
        """ Get last card from tapis joueur for provided 'pile' """
        return self._m_tapis_joueur[the_pile].what_is_card()

    ## Pop last card from tapis joueur for provided 'pile'
    # @return Pop the last card from the given 'pile'
    def pop_last_card(self, the_pile):
        """ Pop last card from tapis joueur for provided 'pile' """
        return self._m_tapis_joueur[the_pile].pop_last_card()

    ## Push a card at correct place
    # @param the_card The card to insert in tapis_joueur
    # @param dest_pile The pile where to insert the card
    # @return True if card is pushed, False otherwise
    def push_card(self, the_card, dest_pile):
        """ Push a card at correct place """
        push_statut = False
        # first look what is the color
        the_color = self.__get_color_of_pile(dest_pile)
        if the_card.get_color() is not the_color:
            # push the card, if color is different and value is +1
            the_value = self.__get_value_of_pile(dest_pile)
            if the_card.get_value() == the_value - 1:
                self._m_tapis_joueur[dest_pile].push_card(the_card)
                push_statut = True
        return push_statut

    ## Retreive the color for a given pile
    # @param pile The pile for which the symbol is requested
    # @return The color of the last card
    def __get_color_of_pile(self, the_pile):
        """ Retreive the color for a given pile """
        output = objet_carte.CarteColor.no_color
        if self._m_tapis_joueur[the_pile]:
            if self._m_tapis_joueur[the_pile].how_many_cards():
                the_card = self._m_tapis_joueur[the_pile].what_is_card()
                output = the_card.get_color()
        return output

    ## Retreive the value of last card of the pile
    # @param pile The pile for which the value is requested
    # @return Value for the last card of given pile, None if pile is empty
    def __get_value_of_pile(self, the_pile):
        """ Retreive the value of last card of the pile """
        output = objet_carte.CarteValeur.carte_empty
        if self._m_tapis_joueur[the_pile]:
            if self._m_tapis_joueur[the_pile].how_many_cards():
                the_card = self._m_tapis_joueur[the_pile].what_is_card()
                output = the_card.get_value()
        return output
