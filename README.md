# solitaire

# description 

Il s'agit d'un jeu de solitaire (52 cartes) developpe en Python en utilisant la POO (Programmation Oriente Objet).
Les 2 fichiers PNG fournissent le diagramme de classes et le diagramme de package.

## Pylint

Ci-dessous l'analyse pylint:
https://balacsoft.gitlab.io/solitaire/pylint/result.txt

## regles

![Solitaire Image](./img/solitaire_zones_de_jeu.png)

L'objectif est de deplacer toutes les cartes vers la zone de resultat
Dans la zone, les cartes doivent etre positionnees par ordre croissant avec le meme symbole.

Dans la zone de jeu il y a 7 piles, avec la premiere carte visible et les autres cachees.
Dans la zone de jeu, les cartes doivent etre positionnees alternativement rouge/noir et par ordre decroissant.

## touches

q=quitter
p=piocher
g=deplacer de la pioche vers resultat
